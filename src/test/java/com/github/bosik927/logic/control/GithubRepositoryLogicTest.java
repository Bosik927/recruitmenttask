package com.github.bosik927.logic.control;

import com.github.bosik927.api.entity.GithubRepository;
import com.github.bosik927.api.entity.InputStreamToJsonMapper;
import com.github.bosik927.logic.boundary.GithubRepositoryEmptinessException;
import com.github.bosik927.logic.boundary.GithubRepositoryIncorrectParametersException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import sun.net.www.protocol.http.HttpURLConnection;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PrepareForTest(GithubRepositoryLogic.class)
public class GithubRepositoryLogicTest {

    @InjectMocks
    GithubRepositoryLogic githubRepositoryLogic;
    @Mock
    InputStreamToJsonMapper inputStreamToJsonMapper;

    private static final String USER_NAME = "Bosik927";
    private static final String USER_REPOSITORY = "HotelManagementService";

    @Test
    public void correctInputStream() throws Exception {
        String initialString = "{\"full_name\":\"Bosik927/HotelManagementService\",\"description\":null," +
                "\"clone_url\":\"https://github.com/Bosik927/HotelManagementService.git\",\"stargazers_count\":1," +
                "\"created_at\":\"2019-05-01T20:55:06Z\",\"param1\":null,\"param2\":null}";
        InputStream targetStream = new ByteArrayInputStream(initialString.getBytes());

        HttpURLConnection httpURLConnection = PowerMockito.mock(HttpURLConnection.class);
        URL url = PowerMockito.mock(URL.class);
        whenNew(URL.class).withAnyArguments().thenReturn(url);
        when(url.openConnection()).thenReturn(httpURLConnection);
        when(httpURLConnection.getInputStream()).thenReturn(targetStream);
        when(inputStreamToJsonMapper.toJson(any())).thenReturn(initialString);

        assertEquals(githubRepositoryLogic.getGithubRepository(USER_NAME, USER_REPOSITORY),
                createSampleCorrectObject());
    }

    @Test(expected = GithubRepositoryEmptinessException.class)
    public void cannotFindContent() throws Exception {
        HttpURLConnection httpURLConnection = PowerMockito.mock(HttpURLConnection.class);
        URL url = PowerMockito.mock(URL.class);
        whenNew(URL.class).withAnyArguments().thenReturn(url);
        when(url.openConnection()).thenReturn(httpURLConnection);
        when(httpURLConnection.getInputStream()).thenThrow(new IOException());

        assertEquals(githubRepositoryLogic.getGithubRepository(USER_NAME, USER_REPOSITORY),
                createSampleCorrectObject());
    }

    @Test(expected = GithubRepositoryIncorrectParametersException.class)
    public void enterIncorrectParameters() throws Exception {
        HttpURLConnection httpURLConnection = PowerMockito.mock(HttpURLConnection.class);
        URL url = PowerMockito.mock(URL.class);
        whenNew(URL.class).withAnyArguments().thenReturn(url);
        when(url.openConnection()).thenReturn(httpURLConnection);
        when(httpURLConnection.getInputStream()).thenThrow(new MalformedURLException());

        assertEquals(githubRepositoryLogic.getGithubRepository(USER_NAME, USER_REPOSITORY),
                createSampleCorrectObject());
    }

    private GithubRepository createSampleCorrectObject() {
        GithubRepository githubRepository = new GithubRepository();
        githubRepository.setCreatedAt("2019-05-01T20:55:06Z");
        githubRepository.setCloneUrl("https://github.com/Bosik927/HotelManagementService.git");
        githubRepository.setFullName("Bosik927/HotelManagementService");
        githubRepository.setStars(1);
        return githubRepository;
    }
}
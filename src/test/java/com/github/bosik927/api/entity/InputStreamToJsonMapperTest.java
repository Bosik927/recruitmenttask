package com.github.bosik927.api.entity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PrepareForTest(InputStreamToJsonMapper.class)
public class InputStreamToJsonMapperTest {

    @InjectMocks
    InputStreamToJsonMapper inputStreamToJsonMapper;

    @Test
    public void correctInputStream() {
        String initialString = "{\"fullName\":\"Bosik927/HotelManagementService\",\"description\":null," +
                "\"cloneUrl\":\"https://github.com/Bosik927/HotelManagementService.git\",\"stars\":1," +
                "\"createdAt\":\"2019-05-01T20:55:06Z\"}";
        InputStream targetStream = new ByteArrayInputStream(initialString.getBytes());

        assertEquals(initialString, inputStreamToJsonMapper.toJson(targetStream));
    }

    @Test
    public void correctMoreAdvanceInputStream() {
        String initialString = "{\"fullName\":\"Bosik927/HotelManagementService\",\"description\":null," +
                "\"cloneUrl\":\"https://github.com/Bosik927/HotelManagementService.git\",\"stars\":1," +
                "\"createdAt\":\"2019-05-01T20:55:06Z\",\"param1\":null,\"param2\":null}";
        InputStream targetStream = new ByteArrayInputStream(initialString.getBytes());

        assertEquals(initialString, inputStreamToJsonMapper.toJson(targetStream));
    }

    @Test
    public void incorrectInputStream() throws Exception {
        String initialString = "asdasd";
        InputStream targetStream = new ByteArrayInputStream(initialString.getBytes());
        BufferedReader bf = mock(BufferedReader.class);
        when(bf.readLine()).thenThrow(new IOException());

        whenNew(BufferedReader.class).withAnyArguments().thenReturn(bf);

        assertEquals("{}", inputStreamToJsonMapper.toJson(targetStream));
    }
}
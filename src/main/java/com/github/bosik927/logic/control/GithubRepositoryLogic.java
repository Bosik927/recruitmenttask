package com.github.bosik927.logic.control;

import com.github.bosik927.api.entity.GithubRepository;
import com.github.bosik927.api.entity.InputStreamToJsonMapper;
import com.github.bosik927.logic.boundary.GithubRepositoryEmptinessException;
import com.github.bosik927.logic.boundary.GithubRepositoryIncorrectParametersException;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriTemplate;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

@Component
public class GithubRepositoryLogic {

    private static final String BASE_GITHUB_URL = "https://api.github.com";
    private static final String GITHUB_REPOS_TEMPLATE = "/repos/{owner}/{repository-name}";
    private static final Logger LOGGER = LoggerFactory.getLogger(GithubRepositoryLogic.class);

    private InputStreamToJsonMapper inputStreamToJsonMapper;

    @Autowired
    public GithubRepositoryLogic(InputStreamToJsonMapper inputStreamToJsonMapper) {
        this.inputStreamToJsonMapper = inputStreamToJsonMapper;
    }

    public GithubRepository getGithubRepository(String owner, String repositoryName)
            throws GithubRepositoryIncorrectParametersException, GithubRepositoryEmptinessException {
        try {
            return getGithubRepository(
                    createFullRelativeUrl(
                            createBasicRelativeUrl(owner, repositoryName)));
        } catch (MalformedURLException e) {
            LOGGER.error("Problem with creation url!", e);
            throw new GithubRepositoryIncorrectParametersException(e);
        } catch (IOException e) {
            LOGGER.warn("Do not found repository with parameters owner:" + owner
                    + " repository name:" + repositoryName, e);
            throw new GithubRepositoryEmptinessException(e);
        }
    }

    private GithubRepository getGithubRepository(URL fullRelativeUrl) throws IOException {
        HttpURLConnection con = (HttpURLConnection) fullRelativeUrl.openConnection();
        con.setRequestMethod("GET");
        return new Gson().fromJson(inputStreamToJsonMapper.toJson(con.getInputStream()),
                GithubRepository.class);
    }

    private URL createFullRelativeUrl(URL relativeUrl) throws MalformedURLException {
        return new URL(new URL(BASE_GITHUB_URL), relativeUrl.toString());
    }

    private URL createBasicRelativeUrl(String owner, String repositoryName) throws MalformedURLException {
        Map<String, String> attributes = new HashMap<>();
        attributes.put("owner", owner);
        attributes.put("repository-name", repositoryName);

        return new URL(new URL(BASE_GITHUB_URL),
                new UriTemplate(GITHUB_REPOS_TEMPLATE).expand(attributes).toString());
    }
}
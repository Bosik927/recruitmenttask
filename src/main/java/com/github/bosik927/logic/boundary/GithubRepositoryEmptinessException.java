package com.github.bosik927.logic.boundary;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Did not find repository with entered parameters")
public class GithubRepositoryEmptinessException extends Exception {

    public GithubRepositoryEmptinessException(Throwable cause) {
        super(cause);
    }
}
package com.github.bosik927.logic.boundary;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Some parameters are invalid!")
public class GithubRepositoryIncorrectParametersException extends Exception {

    public GithubRepositoryIncorrectParametersException(Throwable cause) {
        super(cause);
    }
}
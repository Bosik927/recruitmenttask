package com.github.bosik927.api.entity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

@Component
public class InputStreamToJsonMapper {

    private static final String EMPTY_JSON = "{}";
    private static final Logger LOGGER = LoggerFactory.getLogger(InputStreamToJsonMapper.class);

    public String toJson(InputStream inputStream) {
        BufferedReader br = new BufferedReader(new InputStreamReader((inputStream)));
        StringBuilder jsonBuilder = new StringBuilder();
        String output;

        try {
            while ((output = br.readLine()) != null) {
                jsonBuilder.append(output);
            }
        } catch (IOException e) {
            LOGGER.warn("Problem with reading inputStream!", e);
            return EMPTY_JSON;
        }

        return jsonBuilder.toString();
    }
}
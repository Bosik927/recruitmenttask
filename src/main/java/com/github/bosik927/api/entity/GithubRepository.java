package com.github.bosik927.api.entity;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;
import java.util.Optional;

public class GithubRepository {

    @SerializedName("full_name")
    private String fullName;
    private Optional<String> description;
    @SerializedName("clone_url")
    private String cloneUrl;
    @SerializedName("stargazers_count")
    private int stars;
    @SerializedName("created_at")
    private String createdAt;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Optional<String> getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = Optional.of(description);
    }

    public String getCloneUrl() {
        return cloneUrl;
    }

    public void setCloneUrl(String cloneUrl) {
        this.cloneUrl = cloneUrl;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GithubRepository)) return false;
        GithubRepository that = (GithubRepository) o;
        return getStars() == that.getStars() &&
                Objects.equals(getFullName(), that.getFullName()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getCloneUrl(), that.getCloneUrl()) &&
                Objects.equals(getCreatedAt(), that.getCreatedAt());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFullName(), getDescription(), getCloneUrl(), getStars(), getCreatedAt());
    }
}
package com.github.bosik927.api.control;

import com.github.bosik927.api.entity.GithubRepository;
import com.github.bosik927.logic.boundary.GithubRepositoryEmptinessException;
import com.github.bosik927.logic.boundary.GithubRepositoryIncorrectParametersException;
import com.github.bosik927.logic.control.GithubRepositoryLogic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/repositories", produces = {"application/json"})
public class GithubController {

    private GithubRepositoryLogic githubRepositoryLogic;

    @Autowired
    public GithubController(GithubRepositoryLogic githubRepositoryLogic) {
        this.githubRepositoryLogic = githubRepositoryLogic;
    }

    @GetMapping(path = "/{owner}/{repository-name}")
    public GithubRepository getGithubRepository(@PathVariable String owner,
                                                @PathVariable("repository-name") String repositoryName)
            throws GithubRepositoryIncorrectParametersException, GithubRepositoryEmptinessException {
        return githubRepositoryLogic.getGithubRepository(owner, repositoryName);
    }
}